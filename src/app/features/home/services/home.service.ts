import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {CreateUserRequest} from "../../register/models/CreateUserRequest";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class HomeService {

  fetch_user_url="http://localhost:3000/users";
  add_fav_city = "http://localhost:3000/user/add";
  delete_city_url = "http://localhost:3000/user/city/remove";
  constructor(private readonly httpClient: HttpClient) { }

  fetchUsers(): Observable<any> {
    const response = this.httpClient.get<any>(this.fetch_user_url);
    return response;
  }

  addFavCity(city: string, id: number): Observable<any> {
    console.log(city, id);
    const response = this.httpClient.post<any>(this.add_fav_city, {"id": id, "city": city});
    return response;
  }

  deleteFavCity(city: string, id: any): Observable<any> {
    console.log();
    const response = this.httpClient.delete<any>(this.delete_city_url, {body: {"id": id, "city": city}});
    return response;
  }
}
