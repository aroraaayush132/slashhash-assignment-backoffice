import { Component, OnInit } from '@angular/core';
import {HomeService} from "./services/home.service";

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  // users: Promise<void> | undefined;
  users: any = [];
  apiKey = "8d83e6ca960c87703281c0bd3ec0b807";
  city: string = "";

  constructor(private homeService: HomeService) { }
  ngOnInit(): void {
  }

  getUsers = async () => {
    await this.homeService.fetchUsers().subscribe(res => {
      this.users = res;
      console.log(this.users);
    });
  };

  addFavCity = async (city: string, id: number) => {
    console.log(typeof id);
    await this.homeService.addFavCity(city, id).subscribe(res => {
      this.city = "";
    });
  };

  deleteFavCity = async (city: string, id: any) => {
    await this.homeService.deleteFavCity(city, id).subscribe(res => {
    });
  }

}
