import { Component, OnInit } from '@angular/core';
import {RegisterService} from "./services/register.service";
import {CreateUserRequest} from "./models/CreateUserRequest";
import {CreateUserResponse} from "./models/CreateUserResponse";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  name: string = "";
  email: string = "";
  contact: string = "";
  gender: string = "";

  createUserRequest: CreateUserRequest = new CreateUserRequest();

  constructor(private registerService: RegisterService) {
  }

  ngOnInit(): void {
  }

  async createUser() {
    this.createUserRequest.name = this.name;
    this.createUserRequest.contact = this.contact;
    this.createUserRequest.email = this.email;
    this.createUserRequest.gender = this.gender;
    await this.registerService.createUser(this.createUserRequest);
  }
}
