export class CreateUserRequest {
  name: string = "";
  email: string = "";
  contact: string = "";
  gender: string = "";
}
