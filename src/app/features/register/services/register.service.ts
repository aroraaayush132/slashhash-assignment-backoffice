import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { CreateUserRequest } from '../models/CreateUserRequest';

@Injectable({
  providedIn: 'root',
})
export class RegisterService {
  create_user_url="http://localhost:3000/signup";
  constructor(private http: HttpClient) { }

  async createUser(createUserRequest:CreateUserRequest) {
    console.log(createUserRequest);
    this.http.post<any>(this.create_user_url, createUserRequest).subscribe((res) => {
      console.log(res);
      window.alert(res.message);
    });
  }

}
