import { Component, OnInit } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';

import {ApiEndpoints, AppRouteUrls} from 'src/app/config';
import {environment} from "../../../environments/environment";
import {HttpClient} from "@angular/common/http";


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  // Application URLs.
  appUrls = AppRouteUrls;

  // Flags to show if page is active.
  homeIsActive = false;
  faqIsActive = false;

  // Environment
  environmentName = "";
  isLogin: any;

  constructor(private readonly router: Router, private http: HttpClient) { }

  ngOnInit(): void {
    this.routeStatus();
    this.setDefaultValues();
    this.environmentName = environment.name;
    this.isLogin = localStorage.getItem("is_login");
  }

  /**
   * Route status to check the active links and assigning the path.
   */


  private routeStatus() {
    this.router.events.subscribe(event => {
      if (event instanceof NavigationEnd) {
        this.homeIsActive = this.router.url.startsWith(`/${this.appUrls.Home.BASE_URL}`);
        this.faqIsActive = this.router.url.startsWith(`/${this.appUrls.Faq.BASE_URL}`);
      }
    });
  }

  /**
   * Set default values.
   */
  private setDefaultValues(): void {
    this.homeIsActive = false;
    this.faqIsActive = false;
  }
}
