import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {RouterModule} from '@angular/router';
import {InvalidPageComponent} from "./components/invalid-page/invalid-page.component";
import {HeaderComponent} from "./header/header.component";

@NgModule({
  declarations: [HeaderComponent, InvalidPageComponent],
  imports: [
    CommonModule,
    RouterModule,
  ],
  exports: [HeaderComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class CoreModule { }
