import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AppRouteUrls} from "./config";
import {HomeComponent} from "./features/home/home.component";
import {FaqComponent} from "./features/faq/faq.component";
import {RegisterComponent} from "./features/register/register.component";

const routes: Routes = [
  { path: AppRouteUrls.Home.BASE_URL, component: HomeComponent },
  { path: AppRouteUrls.Register.BASE_URL, component: RegisterComponent },
  { path: AppRouteUrls.Faq.BASE_URL, component: FaqComponent },
  { path: '**', redirectTo: AppRouteUrls.DEFAULT_URL}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
