import { environment } from 'src/environments/environment';

export abstract class Register {
  static readonly BASE_URL = environment.Api + 'user/signup';
}

export abstract class ApiEndpoints {
    static readonly Register = Register;
}
