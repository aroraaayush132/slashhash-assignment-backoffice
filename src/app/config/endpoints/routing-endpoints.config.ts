export abstract class HomeUrls {
    static readonly BASE_URL = '';
}

export abstract class FaqUrls {
    static readonly BASE_URL = 'faq';
}

export abstract class LoginUrl {
  static readonly BASE_URL = 'login';
}

export abstract class RegisterUrl {
  static readonly BASE_URL = 'register';
}

export abstract class AppRouteUrls {
    static readonly Home = HomeUrls;
    static readonly Faq = FaqUrls;
    static readonly Login = LoginUrl;
    static readonly Register = RegisterUrl;

    static readonly DEFAULT_URL = '';
}
